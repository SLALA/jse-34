package ru.t1.strelcov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlFasterXMLLoadRequest extends AbstractUserRequest {

    public DataYamlFasterXMLLoadRequest(@Nullable final String token) {
        super(token);
    }

}
