package ru.t1.strelcov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException(@NotNull String message) {
        super("Error: Access is denied. " + message);
    }

    public AccessDeniedException() {
        super("Error: Access is denied.");
    }

}
