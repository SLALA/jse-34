package ru.t1.strelcov.tm.api.service;

import lombok.SneakyThrows;

public interface IDataService {

    @SneakyThrows
    void saveBackup();

    @SneakyThrows
    void loadBackup();

    @SneakyThrows
    void saveDataBase64();

    @SneakyThrows
    void saveDataBinary();

    @SneakyThrows
    void saveDataJsonFasterXml();

    @SneakyThrows
    void saveDataJsonJAXB();

    @SneakyThrows
    void saveDataXmlFasterXml();

    @SneakyThrows
    void saveDataXmlJAXB();

    @SneakyThrows
    void saveDataYamlFasterXml();

    @SneakyThrows
    void loadDataBase64();

    @SneakyThrows
    void loadDataBinary();

    @SneakyThrows
    void loadDataJsonFasterXml();

    @SneakyThrows
    void loadDataJsonJAXB();

    @SneakyThrows
    void loadDataXmlFasterXml();

    @SneakyThrows
    void loadDataXmlJAXB();

    @SneakyThrows
    void loadDataYamlFasterXml();

}
